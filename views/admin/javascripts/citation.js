jQuery(document).ready(function() {
    Omeka.wysiwyg({
        selector: '#citation_items, #citation_collections, #citation_files',
        menubar: 'edit view insert format table',
        toolbar: ["bold italic underline strikethrough | sub sup | forecolor backcolor | link | anchor | formatselect code | superscript subscript", "hr | alignleft aligncenter alignright alignjustify | indent outdent | bullist numlist | table | pastetext, pasteword | charmap | media | image"],
        plugins: "lists,link,code,paste,autoresize,media,charmap,hr,table,textcolor,image, anchor",
        browser_spellcheck: true
    });
    // Add or remove TinyMCE control.
/*
    jQuery('#simple-pages-use-tiny-mce').click(function() {
        if (jQuery(this).is(':checked')) {
*/
            tinyMCE.EditorManager.execCommand('mceAddEditor', true, 'eman-citation-text');
/*
        } else {
            tinyMCE.EditorManager.execCommand('mceRemoveEditor', true, 'simple-pages-text');
        }
    });
*/

});