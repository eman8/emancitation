<?php

queue_js_file('vendor/tinymce/tinymce.min');
queue_js_file('citation');

head_css();
echo head(array('title' => 'Eman Citation'));

echo flash();
?>

<!-- <script type="text/javascript" src="<?php echo WEB_ROOT; ?>/plugins/EmanIndex/javascripts/emanindex.js"></script> -->
<script>
  $ = jQuery;
  $(document).ready(function() {
  	$('#citation-tokens legend').click(function() {
      $('#citation-tokens').children().not('legend').toggle();
  	});
	});
</script>

<style>
.citation-preview {
  font-family: "Crimson", sans-serif;
  font-weight: 300;
  font-size: 16px;
  line-height: 24px;
  word-wrap: break-word;
  padding:1em;
  margin-bottom:1em;
}
#EmanCitation #submit {
  float:right;
}
#citation-tokens {
	display: block;
	border: 2px solid #9d5b41;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	border-radius: 8px;
	padding: 10px;
	clear: both;
	margin-bottom: 20px;
	font-size:.8em;
}
#citation-tokens legend {
  cursor: pointer;
  margin:0;
}
#citation-tokens ul {
  margin-top:0;
}
#citation-tokens ul li:nth-last-child(3) {
  margin-top:2em;
}
</style>
<div id='citation-menu'>
  <a class='add button small green' href='<?php echo WEB_ROOT; ?>/admin/emancitation'>Citation</a>
  <a class='add button small green' href='<?php echo WEB_ROOT; ?>/admin/emancitationfields'>Champs</a>
</div>
<br /><br /><br />
<fieldset id="citation-tokens">
  <legend>Jetons disponibles</legend>
  <?= $tokens ?>
</fieldset>

<h3>Saisissez les modèles de vos blocs citation ci-dessous :</h3>
<em><strong>Vous devez ajouter les séparateurs (virgule, point virgule) et mettre vous même les retours à la ligne. Attention, si vous ajoutez une virgule dans le modèle, elle apparaitra tout le temps - même si le champ précédent n'est pas rempli.</strong></em>
<?php
echo $content;
?>
<br /><br />
<?php
echo $preview;
echo foot();
?>

