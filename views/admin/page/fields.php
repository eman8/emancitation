<?php

// queue_css_file('eman-plugin');
head_css();
echo head(array('title' => 'Eman Citation'));

echo flash();
?>

<script type="text/javascript" src="<?php echo WEB_ROOT; ?>/plugins/EmanIndex/javascripts/emanindex.js"></script>
<div id='citation-menu'>
  <a class='add button small green' href='<?php echo WEB_ROOT; ?>/admin/emancitation'>Citation</a>
  <a class='add button small green' href='<?php echo WEB_ROOT; ?>/admin/emancitationfields'>Champs</a>
</div>
<br /><br />
<p>Champs disponibles pour les jetons de citation</p>
<?php
echo $form;

echo foot();
?>

