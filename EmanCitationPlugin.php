<?php

/*
 * E-man Plugin
 *
 * Functions to customize Omeka for the E-man Project
 *
 */

class EmanCitationPlugin extends Omeka_Plugin_AbstractPlugin
{

  protected $_hooks = array(
  	'define_routes',
  	'define_acl',
  );

  protected $_filters = array(
  	'admin_navigation_main',
  );

  function hookDefineRoutes($args)
  {
		$router = $args['router'];
 		$router->addRoute(
 				'eman_citation',
 				new Zend_Controller_Router_Route(
 						'emancitation',
 						array(
 								'module' => 'eman-citation',
 								'controller'   => 'page',
 								'action'       => 'citation',
 						)
 				)
 		);
 		$router->addRoute(
 				'eman_citation_fields',
 				new Zend_Controller_Router_Route(
 						'emancitationfields',
 						array(
 								'module' => 'eman-citation',
 								'controller'   => 'page',
 								'action'       => 'fields',
 						)
 				)
 		);
  }

  /**
   * Add the pages to the public main navigation options.
   *
   * @param array Navigation array.
   * @return array Filtered navigation array.
   */
  public function filterAdminNavigationMain($nav)
  {
    $nav[] = array(
                    'label' => __('Eman Citation'),
                    'uri' => url('emancitation'),
    								'resource' => 'EmanCitation_Page',
                  );
    return $nav;
  }

  function hookDefineAcl($args)
  {
  	$acl = $args['acl'];
  	$EmanCitationAdmin = new Zend_Acl_Resource('EmanCitation_Page');
  	$acl->add($EmanCitationAdmin);
  }


  static function citationTokens($contentId, $type = 'item') {

    $db = get_db();

  	switch ($type) {
  		case 'item' :
  			$citation = get_option('eman_citation');
  			$content = get_record_by_id('Item', $contentId);
  			$url = '/items/show/';
  			break;
  		case 'collection' :
  			$citation = get_option('eman_citation_collections');
  			$content = get_record_by_id('Collection', $contentId);
  			$url = '/collections/show/';
  			break;
  		case 'file' :
  			$citation = get_option('eman_citation_files');
  			$content = get_record_by_id('File', $contentId);
  			$url = '/files/show/';
  			break;
  	}

    $fields = get_option('emancitation_fields');

    $tokens = EmanCitationPlugin::getTokens();

    $replacements = $keywords = [];
    $i = 0;
    if (! is_array($tokens)) : return ''; endif;
    foreach ($tokens as $name => $value) {
      $keywords[$i] = '{' . $name . '}';
      try {
        $meta = metadata($content, $value, array('delimiter' => ' ; '));
      } catch (Exception $e) {
        $meta = "Jeton indisponible pour $type";
      }
      $replacements[$i] = $meta;
      $i++;
    }
    $keywords[$i + 1] = '{url}';
    $url = WEB_ROOT . $url . $contentId;
    $replacements[$i + 1] = "<a href='$url'>$url</a>";
    $keywords[$i + 2] = '{consulte}';
    $replacements[$i + 2] = date('d/m/Y');
    if ($content->original_filename) {
      $keywords[$i + 3] = '{nomfichier}';
      $replacements[$i + 3] = $content->original_filename;
    }
  	$citation = nl2br(str_replace($keywords, $replacements, $citation));

  	return $citation;
  }

  public function getTokens($mode = 'tokens') {
    $db = get_db();
    $fields = unserialize(get_option('emancitation_fields'));
    if (! $fields) : return "Choisissez les jetons à rendre disponibles sur <a href='" . WEB_ROOT . "/admin/emancitationfields'>cette page</a>."; endif;
    uksort($fields, function($a, $b) {return $a > $b;});
    $tokenList = '<ul>';
    $tokens = [];
    foreach ($fields as $fieldId => $on) {
      $meta = $db->query("SELECT s.name setName, e.name elementName FROM `$db->Elements` e LEFT JOIN `$db->ElementSets` s ON e.element_set_id = s.id WHERE e.id = ?", [substr($fieldId, -2)])->fetchObject();
      if ($meta) {
        $tokenList .= '<li><span style="font-weight:bold;">{' . EmanCitationPlugin::accent2ascii($meta->elementName) . '}</span> (' . $meta->setName . ' - ' . __($meta->elementName) . ')</li>';
        $tokens[EmanCitationPlugin::accent2ascii($meta->elementName)] = [$meta->setName, $meta->elementName];
      }
    }
    $tokenList .= '<li><span style="font-weight:bold;">{url}</span> (URL du contenu)</li>';
    $tokenList .= '<li><span style="font-weight:bold;">{date-consulte}</span> (Date de consultation = Date courante)</li>';
    $tokenList .= '<li><span style="font-weight:bold;">{nomfichier}</span> (Nom du fichier original avec extension) (uniquement pour les fichiers)</li>';
    $tokenList .= '</ul>';
    if ($mode == 'list') {
      return $tokenList;
    }
    return $tokens;
  }

  public function accent2ascii(string $str, string $charset = 'utf-8'): string
  {
      $str = htmlentities($str, ENT_NOQUOTES, $charset);

      $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
      $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
      $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
      $str = str_replace([' ', '/'], '_', $str);
      return strtolower($str);
  }
}
