<?php
class EmanCitation_PageController extends Omeka_Controller_AbstractActionController
{
	public function getCitationForm()
	{

		$form = new Zend_Form();
		$form->setName('EmanCitation');

		$title = new Zend_Form_Element_Note('citation_items_title');
		$title->setValue("<h4>Items : Texte de la citation</h4>");
		$form->addElement($title);
		$citation = new Zend_Form_Element_Textarea('citation_items');
		$citation->setLabel('Items : Texte de la citation');
		$citation->setValue(get_option('eman_citation'));
		$citation->setAttribs(array('rows'=> 5, 'cols' => 30));
		$form->addElement($citation);

		$title = new Zend_Form_Element_Note('citation_files_title');
		$title->setValue("<h4>Fichiers : Texte de la citation</h4>");
		$form->addElement($title);
		$citation = new Zend_Form_Element_Textarea('citation_files');
		$citation->setLabel('Fichiers : Texte de la citation');
		$citation->setValue(get_option('eman_citation_files'));
		$citation->setAttribs(array('rows'=> 5, 'cols' => 30));
		$form->addElement($citation);

		$title = new Zend_Form_Element_Note('citation_collections_title');
		$title->setValue("<h4>Collections : Texte de la citation</h4>");
		$form->addElement($title);
		$citation = new Zend_Form_Element_Textarea('citation_collections');
		$citation->setLabel('Collections : Texte de la citation');
		$citation->setValue(get_option('eman_citation_collections'));
		$citation->setAttribs(array('rows'=> 5, 'cols' => 30));
		$form->addElement($citation);

		$blank = new Zend_Form_Element_Note('citation_blank_line');
		$blank->setValue("<br />");
		$form->addElement($blank);

		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Sauvegarder les citations');
		$form->addElement($submit);

		// Prettify form
		$elements = $form->getElements();
		foreach ($elements as $elem) {
			$elem->setDecorators(array(
					'ViewHelper',
					// 					array( 'Label', array('placement' => 'prepend')),
					array('HtmlTag', array('tag' => 'div', 'class' => 'eman-citation')),
			));
		}

		return $form;
	}

  public function getFieldsForm() {
    $values = unserialize(get_option('emancitation_fields'));
		$db = get_db();
		$query = "SELECT DISTINCT (name) name, id FROM `$db->Elements` ORDER BY name";
		$elements = $db->query($query)->fetchAll();
		$fields = [];
		foreach ($elements as $i => $element) {
  		$fields[$element['id']] = $element['name'];
    }

		$form = new Zend_Form();
		$form->setName('FieldList');

  	$checkall = new Zend_Form_Element_Checkbox('checkall');
  	$checkall->setLabel('Tout cocher');
  	$checkall->setAttrib('title', 'Tout cocher');
  	$checkall->setValue('checkall');
  	$form->addElement($checkall);

		foreach ($fields as $id => $field) {
 			$lefield = new Zend_Form_Element_Checkbox('field_' . $id);
 			$lefield->setLabel('(' . EmanCitationPlugin::accent2ascii($field) . ') ' . $field);
 			if (isset($values['field_' . $id])) {
   			$lefield->setValue($values['field_' . $id] == 1);
 			} else {
   			$lefield->setValue(false);
 			}
    	$lefield->setAttrib('class', 'check-available');
 			$form->addElement($lefield);
		}

    $submit = new Zend_Form_Element_Submit('submit');
    $submit->setLabel('Sauvegarder les champs');
    $form->addElement($submit);

		$form = $this->prettifyForm($form);
    return $form;
  }

	private function prettifyForm($form) {
		// Prettify form
		$form->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'table')),
				'Form'
		));
		$form->setElementDecorators(array(
				'ViewHelper',
				'Errors',
				array(array('data' => 'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td', 'style' => 'text-align:right;float:right;')),
				array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
		));
		return $form;
	}

	public function citationAction()
	{
		$form = $this->getCitationForm();

		if ($this->_request->isPost()) {
			$formData = $this->_request->getPost();
			if ($form->isValid($formData)) {
				// Tri des blocs avant sauvegarde
				$citation = $form->getValues();
				// Save setting for overriding on items/show
				set_option('eman_citation', $citation['citation_items']);
				// Save setting for overriding on items/show
				set_option('eman_citation_files', $citation['citation_files']);
				// Save setting for overriding on items/show
				set_option('eman_citation_collections', $citation['citation_collections']);

				// Sauvegarde form dans DB
				$db = get_db();

				$this->_helper->flashMessenger('Modèles de citations sauvegardés.');
			}
		}
		$this->view->content = $form;
		$db = get_db();
		$maxItemId = $db->query("SELECT MAX(id) id FROM `$db->Items`")->fetchAll();
		$maxCollectionId = $db->query("SELECT MAX(id) id FROM `$db->Collections`")->fetchAll();
		$maxFileId = $db->query("SELECT MAX(id) id FROM `$db->Files`")->fetchAll();
		$citations = '';
		if ($maxItemId[0]['id']) {
			$citations .= "<em>Voici un aper&ccedil;u de la citation telle qu'elle apparaîtra sur la page item :</em><br /><br />";
			$citations .= "<div class='citation-preview' style='background:#eee;'>" . EmanCitationPlugin::citationTokens($maxItemId[0]['id']) . "</div>";
		} else {
			$citations .= "Créez au moins un item pour obtenir une prévisualisation.";
		}
		if ($maxCollectionId[0]['id']) {
			$citations .= "<em>Voici un aper&ccedil;u de la citation telle qu'elle apparaîtra sur la page collection :</em><br /><br />";
			$citations .= "<div class='citation-preview' style='background:#eee;'>" . EmanCitationPlugin::citationTokens($maxCollectionId[0]['id'], 'collection') . "</div>";
		} else {
			$citations .= "Créez au moins une collection pour obtenir une prévisualisation.";
		}
		if ($maxFileId[0]['id']) {
			$citations .= "<em>Voici un aper&ccedil;u de la citation telle qu'elle apparaîtra sur la page fichier :</em><br /><br />";
			$citations .= "<div class='citation-preview' style='background:#eee;'>" . EmanCitationPlugin::citationTokens($maxFileId[0]['id'], 'file') . "</div>";
		} else {
			$citations .= "Créez au moins un fichier pour obtenir une prévisualisation.";
		}
    $this->view->preview = $citations;
		$this->view->tokens = EmanCitationPlugin::getTokens('list');
		$this->render();
	}

	public function fieldsAction()
	{
  	$form = $this->getFieldsForm();
		if ($this->_request->isPost()) {
			$formData = $this->_request->getPost();
			if ($form->isValid($formData)) {
  			$values = $form->getValues();
  			unset ($values['checkall']);
				set_option('emancitation_fields', serialize(array_filter($values, function($v) {return $v == 1;}, ARRAY_FILTER_USE_BOTH)));
        $this->_helper->flashMessenger('Champs disponibles pour Eman Citations sauvegardés.');
		  }
		}
		$this->view->form = $form;
  }

}